# README #

Hi 
This is a minimalist Auralux like game written in Visual Studio 2015, C# 6 and WPF. 
I developed it for a University project.

The source format is cheked by StyleCop with some rule disabled.

In the DOCS_BIN.zip file there is a doxygen documentation, the compiled binary (exe) for 64 bit, the configuration file and a hungarian user manual.

Please feel free to study, play and modify for your purpose.

Best regards,
Mate Kurtos