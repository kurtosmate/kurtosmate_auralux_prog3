﻿//-----------------------------------------------------------------------
// <copyright file="AppConfig.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------

namespace Auralux_kurtosmate_alls24
{
    using System;
    using System.Configuration;
    using System.Windows.Media;

    /// <summary>
    /// Values from App.config
    /// </summary>
    public static class AppConfig
    {
        /// <summary>
        /// Human Color
        /// </summary>
        private static Color humanColor;

        /// <summary>
        /// Gets Human Color property
        /// </summary>
        public static Color HumanColor
        {
            get { return humanColor; }
        }

        /// <summary>
        /// Computer Color
        /// </summary>
        private static Color computerColor;

        /// <summary>
        /// Gets Computer Color property
        /// </summary>
        public static Color ComputerColor
        {
            get { return computerColor; }
        }

        /// <summary>
        /// Unit Size
        /// </summary>
        private static int unitSize;

        /// <summary>
        /// Gets Unit Size property
        /// </summary>
        public static int UnitSize
        {
            get { return unitSize; }
        }

        /// <summary>
        /// Max unit number
        /// </summary>
        private static int maxUnitNumber;

        /// <summary>
        /// Gets Max unit number property
        /// </summary>
        public static int MaxUnitNumber
        {
            get { return maxUnitNumber; }
        }

        /// <summary>
        /// Planet size
        /// </summary>
        private static int planetSize;

        /// <summary>
        /// Gets Planet size property
        /// </summary>
        public static int PlanetSize
        {
            get { return planetSize; }
        }

        /// <summary>
        /// Planet number
        /// </summary>
        private static int planetNumber;

        /// <summary>
        /// Gets Planet number property
        /// </summary>
        public static int PlanetNumber
        {
            get { return planetNumber; }
        }

        /// <summary>
        /// Spawn time
        /// </summary>
        private static int spawnTime;

        /// <summary>
        /// Gets Spawn time property
        /// </summary>
        public static int SpawnTime
        {
            get { return spawnTime; }
        }

        /// <summary>
        /// Computer min think time
        /// </summary>
        private static int computerMinThinkTime;

        /// <summary>
        /// Gets Computer min think time property
        /// </summary>
        public static int ComputerMinThinkTime
        {
            get { return computerMinThinkTime; }
        }

        /// <summary>
        /// Computer max think time
        /// </summary>
        private static int computerMaxThinkTime;

        /// <summary>
        /// Gets Computer max think time property
        /// </summary>
        public static int ComputerMaxThinkTime
        {
            get { return computerMaxThinkTime; }
        }

        /// <summary>
        /// Computer max moving units divisor
        /// </summary>
        private static int computerMaxMovingUnitsDivisor;

        /// <summary>
        /// Gets Computer max moving units divisor property
        /// </summary>
        public static int ComputerMaxMovingUnitsDivisor
        {
            get { return computerMaxMovingUnitsDivisor; }
        }

        /// <summary>
        /// Read config from App.config
        /// </summary>
        internal static void ReadConfig()
        {
            byte[] humanColorArray = Array.ConvertAll(ConfigurationSettings.AppSettings["HumanColor"].Split(','), b => byte.Parse(b));
            humanColor = Color.FromArgb(255, humanColorArray[0], humanColorArray[1], humanColorArray[2]);

            byte[] computerColorArray = Array.ConvertAll(ConfigurationSettings.AppSettings["ComputerColor"].Split(','), b => byte.Parse(b));
            computerColor = Color.FromArgb(255, computerColorArray[0], computerColorArray[1], computerColorArray[2]);

            int.TryParse(ConfigurationSettings.AppSettings["UnitSize"], out unitSize);
            int.TryParse(ConfigurationSettings.AppSettings["MaxUnitNumber"], out maxUnitNumber);
            int.TryParse(ConfigurationSettings.AppSettings["PlanetSize"], out planetSize);
            int.TryParse(ConfigurationSettings.AppSettings["PlanetNumber"], out planetNumber);
            int.TryParse(ConfigurationSettings.AppSettings["SpawnTime"], out spawnTime);
            int.TryParse(ConfigurationSettings.AppSettings["ComputerMinThinkTime"], out computerMinThinkTime);
            int.TryParse(ConfigurationSettings.AppSettings["ComputerMaxThinkTime"], out computerMaxThinkTime);
            int.TryParse(ConfigurationSettings.AppSettings["ComputerMaxMovingUnitsDivisor"], out computerMaxMovingUnitsDivisor);
        }
    }
}
