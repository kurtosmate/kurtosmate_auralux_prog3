﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------

namespace Auralux_kurtosmate_alls24
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Threading;
    using Auralux_kurtosmate_alls24.Element;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Units property
        /// </summary>
        public ObservableCollection<Unit> Units
        {
            get
            {
                return units;
            }

            set
            {
                units = value;
            }
        }

        /// <summary>
        /// Planets property
        /// </summary>
        public ObservableCollection<Planet> Planets
        {
            get
            {
                return planets;
            }

            set
            {
                planets = value;
            }
        }

        /// <summary>
        /// Indicates Game over
        /// </summary>
        private bool GameOver { get; set; }

        /// <summary>
        /// Number of units
        /// </summary>
        private int UnitCount { get; set; }

        /// <summary>
        /// Random generator
        /// </summary>
        private readonly Random random = new Random();

        /// <summary>
        /// Timer to spawn new units
        /// </summary>
        private DispatcherTimer spawnTimer;

        /// <summary>
        /// Timer to move units
        /// </summary>  
        private DispatcherTimer unitTimer;

        /// <summary>
        /// Timer to check if game has ended and sets the UnitCount
        /// </summary>
        private DispatcherTimer gameControlTimer;

        /// <summary>
        /// Current user interface context. Set to work with multithreaded environment
        /// </summary>
        private readonly SynchronizationContext uiContext = SynchronizationContext.Current;

        /// <summary>
        /// List of units
        /// </summary>
        private ObservableCollection<Unit> units = new ObservableCollection<Unit>();

        /// <summary>
        /// List of planets
        /// </summary>
        private ObservableCollection<Planet> planets = new ObservableCollection<Planet>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class
        /// </summary>
        public MainWindow()
        {
            AppConfig.ReadConfig();
            DataContext = this;
        }

        /// <summary>
        /// Handle if Unit reach a planet
        /// </summary>
        public void CollisionWithPlanet()
        {
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].IsActive)
                {
                    for (int j = 0; j < AppConfig.PlanetNumber; j++)
                    {
                        if (units[i].Owner != planets[j].Owner && units[i].Area.IntersectsWith(planets[j].Area))
                        {
                            planets[j].Owner = units[i].Owner;
                            units[i].IsActive = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handle Unit collisions
        /// </summary>
        public void CollisionWithUnit()
        {
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].IsActive)
                {
                    for (int j = 0; j < units.Count; j++)
                    {
                        if (units[j].IsActive &&
                            units[i].Owner != units[j].Owner &&
                            units[i].Area.IntersectsWith(units[j].Area))
                        {
                            units[i].IsActive = false;
                            units[j].IsActive = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The space is loaded
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameters</param>
        private void BackPlane_Loaded(object sender, RoutedEventArgs e)
        {
            // Create Players
            Player human = new Player(isComputer: false, color: AppConfig.HumanColor);
            Player computer = new Player(isComputer: true, color: AppConfig.ComputerColor);

            // Create Planets
            for (int i = 0; i < AppConfig.PlanetNumber; i++)
            {
                Planets.Add(new Planet(new Point(
                random.Next(AppConfig.PlanetSize, (int)BackPlane.ActualWidth - (AppConfig.PlanetSize * 2)),
                random.Next(AppConfig.PlanetSize, (int)BackPlane.ActualHeight - (AppConfig.PlanetSize * 2)))));
            }

            // Add players to starting planets
            Planets[0].Owner = human;
            Planets[AppConfig.PlanetNumber - 1].Owner = computer;

            // Create timers for game
            unitTimer = new DispatcherTimer(DispatcherPriority.Render);
            unitTimer.Interval = new System.TimeSpan(0, 0, 0, 0, 35);

            spawnTimer = new DispatcherTimer(DispatcherPriority.Normal);
            spawnTimer.Tick += SpawnTimer_Tick;
            spawnTimer.Interval = new TimeSpan(0, 0, AppConfig.SpawnTime);

            gameControlTimer = new DispatcherTimer(DispatcherPriority.Normal);
            gameControlTimer.Tick += OverCheck;
            gameControlTimer.Interval = new TimeSpan(0, 0, 3);

            unitTimer.Start();
            spawnTimer.Start();
            gameControlTimer.Start();

            // Start Computer player
            BackgroundWorker computerPlayer = new BackgroundWorker();
            computerPlayer.DoWork += ComputerPlayer_DoWork;
            computerPlayer.RunWorkerAsync();
        }

        /// <summary>
        /// Computer's game logic
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameter</param>
        private void ComputerPlayer_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!GameOver)
            {
                List<int> planetsToConquer = new List<int>();
                NotMyPlanets(ref planetsToConquer, isComputer: false);

                if (planetsToConquer.Count > 0)
                {
                    // Set the target planet
                    int i = planetsToConquer[random.Next(0, planetsToConquer.Count)];

                    // Set the number of unit to send to target
                    int moveNumberOfUnits = random.Next(
                        1, 1 + (NumberOfMyUnits(isComputer: true) / AppConfig.ComputerMaxMovingUnitsDivisor));

                    int j = 0;
                    while (j < UnitCount && moveNumberOfUnits > 0)
                    {
                        // Unit is owned by computer and currently not in move
                        if (!units[j].Owner.Human && units[j].Destination == units[j].Area.Location)
                        {
                            // Send unit to target
                            units[j].Destination = planets[i].Area.Location;
                            moveNumberOfUnits--;
                        }

                        j++;
                    }
                    // Thinking...
                    Thread.Sleep(random.Next(AppConfig.ComputerMinThinkTime, AppConfig.ComputerMaxThinkTime));
                }
            }
        }

        /// <summary>
        /// Returns a list of indexes which not owned by the player
        /// </summary>
        /// <param name="indexes">Planet indexes</param>
        /// <param name="isComputer">Is the player computer</param>
        /// <returns>Indexes which not owned by the player</returns>
        private List<int> NotMyPlanets(ref List<int> indexes, bool isComputer)
        {
            for (int i = 0; i < AppConfig.PlanetNumber; i++)
            {
                if (!(planets[i].Owner?.Human == isComputer))
                {
                    indexes.Add(i);
                }
            }

            return indexes;
        }

        /// <summary>
        /// Return number of my units
        /// </summary>
        /// <param name="isComputer">Is the player computer</param>
        /// <returns>Number of units</returns>
        private int NumberOfMyUnits(bool isComputer)
        {
            int number = 0;
            for (int i = 0; i < UnitCount; i++)
            {
                if (units[i].IsActive && units[i].Owner.Human != isComputer)
                {
                    number++;
                }
            }

            return number;
        }

        /// <summary>
        /// Check game over
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameters</param>
        private void OverCheck(object sender, System.EventArgs e)
        {
            bool humanHasPlanet = false;
            bool computerHasPlanet = false;

            int i = 0;
            while (i < AppConfig.PlanetNumber && !(humanHasPlanet && computerHasPlanet))
            {
                if (planets[i].Owner != null)
                {
                    if (planets[i].Owner.Human == true)
                    {
                        humanHasPlanet = true;
                    }

                    if (planets[i].Owner.Human == false)
                    {
                        computerHasPlanet = true;
                    }
                }

                i++;
            }

            if (!(humanHasPlanet && computerHasPlanet))
            {
                // Someones has no planets
                GameOver = true;
                spawnTimer.Stop();
                unitTimer.Stop();
                gameControlTimer.Stop();
                var dialogResult = MessageBox.Show(
                    "The game has ended. Start new game?",
                    "Over",
                    MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                }

                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// SpawnTimer Tick handler
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameters</param>
        private void SpawnTimer_Tick(object sender, System.EventArgs e)
        {
            Task.Run(
                () => this.SpwanUnit());
        }

        /// <summary>
        /// Create new Unit if we didn't reached the MaxUnitNumber
        /// </summary>
        private void SpwanUnit()
        {
            // If we don't reached the unit limit yet
            for (int i = 0; i < AppConfig.PlanetNumber; i++)
            {
                if (planets[i].Owner != null)
                {
                    Unit born = new Unit(
                        planets[i].Owner,
                         new Point(
                             planets[i].Area.Location.X + random.Next(0, (int)planets[i].Area.Width),
                             planets[i].Area.Location.Y + random.Next(0, (int)planets[i].Area.Height)));

                    unitTimer.Tick += born.Move;
                    born.PropertyChanged += Unit_PropertyChanged;

                    // Check if has Inactive unit
                    int j = 0;
                    while (j < UnitCount && units[j].IsActive)
                    {
                        j++;
                    }

                    if (j >= UnitCount)
                    {
                        // If we don't reached max unit number, born new one
                        if (UnitCount <= AppConfig.MaxUnitNumber)
                        {
                            // There is no Inactive unit, add a new one
                            uiContext.Send(x => units.Add(born), null);
                        }
                    }
                    else
                    {
                        // Found an Inactive unit, reborns it to the right team
                        uiContext.Send(x => units[j] = born, null);
                    }
                }
            }

            this.UnitCount = units.Count;
        }

        /// <summary>
        /// Unit's property has been changed, check collisions
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameter</param>
        private void Unit_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Task.Run(() => CollisionWithUnit());
            Task.Run(() => CollisionWithPlanet());   
        }
    }
}
